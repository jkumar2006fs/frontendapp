import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IpamModule } from './ipam.module';
import { HomeComponent } from './views/home.component';
import { IpamComponent } from './views/ipam.component';
import { JobsComponent } from './views/jobs.component';
import { SubnetComponent } from './views/subnet.component';

export const ipamRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '', component: IpamComponent, children: [
          { path: '', redirectTo: "home", pathMatch: "full"},
          { path: 'home', component: HomeComponent },
          { path: 'subnets', component: SubnetComponent },
          { path: 'jobs', component: JobsComponent},
          { path: '**', redirectTo: "home", pathMatch: "full" }
        ]
      }
    ]
  },
];

export const IpamRouting: ModuleWithProviders<IpamModule> = RouterModule.forChild(ipamRoutes);
