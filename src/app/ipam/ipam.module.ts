import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgxEchartsModule } from 'ngx-echarts';
import { TreeTableModule } from 'primeng/treetable';
import { SharedModule } from '../shared.module';
import { IpamRouting } from './ipam.routes';
import { SubnetService } from './services/subnet.service';
import { HomeComponent } from './views/home.component';
import { IpamComponent } from './views/ipam.component';
import { SubnetComponent } from './views/subnet.component';
import { JobsComponent } from './views/jobs.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TreeTableModule,
    IpamRouting,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ],
  declarations: [
    HomeComponent,
    IpamComponent,
    SubnetComponent,
    JobsComponent
  ],
  providers: [
    SubnetService
  ]
})
export class IpamModule { }
