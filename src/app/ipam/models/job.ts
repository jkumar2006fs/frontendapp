export class Job {
  id: number;
  reference: string;
  name: string;
  group_Id: number;
  priority_Id: number;
  resolution_SLA: string;
  remarks: string;
  ticket_Id: string;
  created_On: Date;
  created_By: string;
  updated_On: Date;
  updated_By: string;
}

export class JobGroup {
  id: number;
  name: string;
  description: string;
}

export class Priority {
  id: number;
  name: string;
  level: number;
  description: number;
}