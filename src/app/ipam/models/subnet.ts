export class Subnet  {

  id: string;
  address: string;
  mask: string;
  name: string;
  description: string;
  size?: number;
  scanStatus: string;
  lastScannedOn: string;
  available?: number;
  used?: number;
  geometry: any;

}
