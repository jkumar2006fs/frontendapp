import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { forkJoin, Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { Job, JobGroup, Priority } from "../models/job";

@Injectable({ providedIn: 'root' })
export class JobService {
  private url: string = `${environment.apiUrl}/api/Jobs/`;

  constructor(private http: HttpClient) {}

  getAll(): Observable<any> {
    const jobs = this.http.get<Array<Job>>(this.url);
    const jobGroups = this.http.get<Array<JobGroup>>(`${this.url}jobgroups`);
    const priorities = this.http.get<Array<Priority>>(`${this.url}priorities`);
    return forkJoin([jobs, jobGroups, priorities]);
  }

  getAllJobs(): Observable<Array<Job>> {
    return this.http.get<Array<Job>>(this.url);
  }

  getAllJobGroups(): Observable<Array<JobGroup>> {
    return this.http.get<Array<JobGroup>>(`${this.url}jobgroups`);
  }

  getAllPriorities(): Observable<Array<Priority>> {
    return this.http.get<Array<Priority>>(`${this.url}priorities`);
  }

  removeJob(jobId: number) {
    const params = new HttpParams().set('id', jobId);
    return this.http.delete(this.url, { params });
  }
}