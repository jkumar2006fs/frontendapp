import { Component } from '@angular/core';
import { forkJoin } from 'rxjs';
import { Job, JobGroup, Priority } from '../models/job';
import { JobService } from '../services/job.service';

declare var $: any;

@Component({
  templateUrl: './jobs.component.html',
  providers: [JobService]
})

export class JobsComponent {

  jobs: Job[] = [];
  jobGroups: JobGroup[] = [];
  priorities: Priority[] = [];
  job: Job;
  statusMessage = 'No Data Found...';

  constructor(private jobService: JobService) {}

  ngOnInit() {
    $("#ajax-loading").show();
    this.jobService.getAll().subscribe((result: any) => {
      this.jobs = result[0];
      this.jobGroups = result[1];
      this.priorities = result[2];
    },
    (error) => {
      console.log(error);
      $("#ajax-loading").hide();
    },
    () => {
      $("#ajax-loading").hide();
    });
  }

  getAllJobs() {
    $("#ajax-loading").show();
    this.jobService.getAllJobs().subscribe(
      (jobs: Job[]) => {
        this.jobs = jobs;
      },
      (error) => {
        console.log(error);
        $("#ajax-loading").hide();
      });
  }

  remove(id: number) {
    this.jobService.removeJob(id).subscribe((result: number) => {
      if (result === 1) {
        this.getAllJobs();
      }
    });
  }

  getGroupName(id: number): string {
    return this.jobGroups.find(j => j.id === id).name;
  }

  getPriorityName(id: number): string {
    return this.priorities.find(j => j.id === id).name;
  }
}